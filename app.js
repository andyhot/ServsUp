'use strict';

const Hapi = require('hapi');
const r = require('rethinkdb');
const config = require('./utils/config.js');
const server = new Hapi.Server();
const routes = require('./routes');
const onDeath = require('death');
const spawn = require('child_process').spawn;

const workers = [
  spawn('node', [`${__dirname}/workers/httpWorker.js`]),
  spawn('node', [`${__dirname}/workers/emailWorker.js`]),
];

workers.forEach(worker => {
  worker.stdout.on('data', (data) => {
    console.log(data.toString());
  });
  worker.stderr.on('data', (data) => {
    console.log(data.toString());
  });
});

server.connection({
  port: config.appPort,
  routes: {
    cors: true,
  },
});
server.register([
  require('inert'),
  require('h2o2'),
  require('hapi-io')], (err) => {
  if (err) throw err;
});

server.route(routes);

server.start((err) => {
  if (err) throw err;
  console.log('-- Server running at: ' + server.info.uri + ' --');
});


console.log("URL is " + config.rhost);
console.log("URL is " + config.rport);
console.log("URL is " + config.rdb);

r.connect({
    host: config.rhost,
    port: config.rport,
    db: config.rdb
  }, (err, conn) => {
  if (err) throw err;
  getChangeFeed(conn);
});

function getChangeFeed(connection) {
  r.table('servers')
    .changes()
    .run(connection, (err, cursor) => {
      const io = server.plugins['hapi-io'].io;
      cursor.each((err, change) => {
        if (err) throw err;
        io.emit('status_change', change);
      });
    });
}

function cleanUp() {
  console.log('\n-- Cleaning up processes --');
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      workers.forEach(worker => worker.kill('SIGINT'));
      console.log('-- All done exiting --');
      resolve(0);
    }, 500)
  });
}

onDeath((sig, err) => {
  if (err) process.exit(1);
  cleanUp()
    .then(process.exit);
});

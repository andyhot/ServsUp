import React from 'react';
import { Route, IndexRoute, Redirect, IndexRedirect } from 'react-router';

import App from '../containers/App';
import Home from '../containers/Home';
import FourOFour from '../containers/FourOFour';

export default (
  <Route path="/" component={App}>
    <IndexRoute components={{ main: Home }} />
    <Route path="404" components={{ main: FourOFour }} />
    <Route path="*">
      <IndexRedirect to="/404" />
    </Route>
  </Route>
);

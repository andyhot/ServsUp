import React, { PropTypes } from 'react';
import Close from 'react-icons/lib/md/clear';

export default function Modal(props) {
  const { children } = props;
  return (
    <div className="modal">
      <div className="modal-body">
        <button
          type="button"
          className="modal-close"
          onClick={props.onModalClose}
        >
          <Close />
        </button>
        <div className="modal-content">
          {children}
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
};

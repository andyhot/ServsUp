import './stylesheets/main.css';
import React from 'react';
import { render } from 'react-dom';
import routes from './src/router/router';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { store } from './src/store/configureStore';


// init shell
renderShell();

function renderShell() {
    let shell = document.createElement('div');
    shell.className = 'app-shell';
    document.body.appendChild(shell);
    render(
      <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
      </Provider>
    , shell);
}



export default function getChangeType(change) {
  if (!change.old_val) return 'create';
  if (!change.new_val) return 'delete';
  return 'update';
}

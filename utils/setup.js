'use strict';

const r = require('rethinkdb');
const fs = require('fs');

const dbName = 'status_servers';
const tableName = 'servers';
const dbHost = 'localhost';
const dbPort = 28015;

const tables = [
  'servers',
  'subscribers'
];

function createTable(conn) {
  return (table) => (
    new Promise((resolve, reject) => {
      r.db(dbName).tableCreate(table).run(conn, (err, tableObject) => {
        if (err && err.msg === `Table \`status_servers.${table}\` already exists.`) {
          console.log(`-- Table ${table} already exists --`);
          return resolve(table);
        } else if (err) {
          return reject(err);
        }
        console.log(`-- Created table ${table} --`);
        return resolve(table);
      });
    })
  );
}

// Creating database if it doesn't already exists, with all tables
r.connect({
  host: dbHost,
  port: dbPort
}, (err, conn) => {
  r.dbCreate(dbName).run(conn, (dbObject) => {
    r.connect({
      host: dbHost,
      port: dbPort,
      db: dbName
    }, (err, conn) => {
      const promisses = tables.map(createTable(conn));
      Promise.all(promisses)
        .then(() => {
          console.log('-- Done creating database --');
          process.exit(0);
        });
    });
  });
});

const config_template = 'tmpl/config.json';
const config_file = '/config/config.json';

// Creates config file, if it doesn't already exists
if (!fs.existsSync(config_file)) {
  console.log('Copying configuration file');
  fs.createReadStream(config_template).pipe(fs.createWriteStream(config_file));
}

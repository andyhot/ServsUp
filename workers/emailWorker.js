'use strict';

const r = require('rethinkdb');
const config = require('../utils/config.js');
const sendMail = require('../utils/sendMail.js');

r.connect({
    host: config.rhost,
    port: config.rport,
    db: config.rdb
  }, (err, conn) => {
    if (err) throw err;
    console.log('-- Connection established, waiting for changes --');
    getChangeFeed(conn);
});

function getChangeFeed(connection) {
  r.table('servers')
    .changes()
    .run(connection, (err, cursor) => {
      if(err) throw err;
      cursor.each((err, change) => {
        if (err) throw err;
        if (change.old_val && change.old_val.status !== change.new_val.status) {
          sendEmail(connection, change);
        }
      });
    });
}

function getSubscriptions(connection) {
  return new Promise((resolve, reject) => {
    r.table('subscribers')
      .run(connection, (err, cursor) => {
        if(err) throw err;
        cursor.toArray()
          .then(resolve)
          .catch(reject);
      });
  });
}

function sendEmail(connection, change) {
  const newVal = change.new_val;
  getSubscriptions(connection)
    .then(subscribers => {
      if (subscribers.length === 0) return;
      const options = {
        to: subscribers.map(s => s.email).join(', '),
        subject: `[STATUS CHANGE] ${newVal.serverName}'s status changed to ${newVal.status.toUpperCase()}`,
        text: `[STATUS CHANGE] ${newVal.serverName}'s status changed to ${newVal.status.toUpperCase()}`,
      };
      sendMail(options)
        .then((info) => {
          console.log(`-- Message sent: ${info.response} --`);
        })
        .catch(err => {
          throw new Error(err);
        });
    })
    .catch((err) => { throw err; });
}
